package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeNormal( ) {
		boolean var = Palindrome.isPalindrome("racecar");
		assertTrue("Method not is working fine", var);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean var = Palindrome.isPalindrome("Hello");
		assertFalse("Method not is working fine", var);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean var = Palindrome.isPalindrome("Race car");
		assertTrue("Method not is working fine", var);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean var = Palindrome.isPalindrome("Raceacar");
		assertFalse("Method not is working fine", var);
	}	
	
}
